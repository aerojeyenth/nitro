# Nitro

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 9.0.7. NodeJS version 10.19.0


# Features

* The sample data has been hardcoded in the books-api-service. 
* This can be extended to connect to the real api. 
* The user can goup the books data by 'week', 'author' or 'location'.
* A kind of tree view is implemented.
* The list of books grouped according to the selection is displayed. 
* This project implements a Uni directional data flow pattern using the RxJS, similar to Redux. 
* The books data is stored in memory. 
* 100% unit test coverage for all the service, components and branches. 

## Special Inline Text Editor

* Implemented a intuitive inline editor for location, author and description fields, where the user can click and edit. On blur or the keypress enter event the edit will be updated.

## CSS styling

* This project uses Bulma CSS framework for the styles.

# Live Demo

The app is deployed to firebase hosting you can check the live demo at [https://nitro-77be8.web.app](https://nitro-77be8.web.app)

## Development server

Clone the git repo to the local machine. 

Run `npm install` and

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

