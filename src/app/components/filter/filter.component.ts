import { Component, Output, EventEmitter, OnDestroy } from '@angular/core';
import { BehaviorSubject, Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';

@Component({
  selector: 'app-filter',
  templateUrl: './filter.component.html',
  styleUrls: ['./filter.component.scss']
})
export class FilterComponent implements OnDestroy {
  key$: BehaviorSubject<string> = new BehaviorSubject('week');
  private unsubscribe$: Subject<void> = new Subject<void>();
  @Output() key = new EventEmitter<string>();
  constructor() {
    this.key$.pipe(takeUntil(this.unsubscribe$)).subscribe(key => this.key.emit(key));
   }

   ngOnDestroy() {
    // Notify the destroy.
    this.unsubscribe$.next();
    this.unsubscribe$.complete();
   }
}
