import { Component, OnInit, Input } from '@angular/core';
import { GroupedBook, Key } from '../header/header.component';

@Component({
  selector: 'app-group-books',
  templateUrl: './group-books.component.html',
  styleUrls: ['./group-books.component.scss']
})
export class GroupBooksComponent implements OnInit {
  @Input() groupedData: GroupedBook;
  @Input() key: Key;
  constructor() { }

  ngOnInit(): void {
  }

}
