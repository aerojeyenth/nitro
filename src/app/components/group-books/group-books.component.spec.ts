import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GroupBooksComponent } from './group-books.component';

describe('GroupBooksComponent', () => {
  let component: GroupBooksComponent;
  let fixture: ComponentFixture<GroupBooksComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GroupBooksComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GroupBooksComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
