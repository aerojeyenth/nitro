import { Component, Input } from '@angular/core';
import { Book, BooksAPIService } from 'src/app/services/books-api.service';

@Component({
  selector: 'app-book-card',
  templateUrl: './book-card.component.html',
  styleUrls: ['./book-card.component.scss']
})
export class BookCardComponent {
  @Input() book: Book;
  constructor(private bookApiService: BooksAPIService) { }

  onEdit(value: string, book: Book, property: string) {
    const newBook = book;
    newBook[property] = value;
    this.bookApiService.updateBook(newBook);
  }
}
