import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BookCardComponent } from './book-card.component';
import { BooksAPIService, Book } from 'src/app/services/books-api.service';
import { InlineEditComponent } from '../inline-edit/inline-edit.component';

describe('BookCardComponent', () => {
  let component: BookCardComponent;
  let fixture: ComponentFixture<BookCardComponent>;
  let mockBooksAPIService: BooksAPIService;
  const book: Book = {
    id: 5,
    location: 'London',
    time: '1553080742',
    author: 'Happy Manager',
    text: 'A modern PDF annotator that can accommodate all of the cooks in a very busy kitchen is what your employees really need.'
  };

  beforeEach(async(() => {
    // Create a spy for the service
    mockBooksAPIService = jasmine.createSpyObj(['updateBook']);
    TestBed.configureTestingModule({
      declarations: [ BookCardComponent, InlineEditComponent ],
      providers: [{
        provide: BooksAPIService,
        useValue: mockBooksAPIService
    }]})
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BookCardComponent);
    component = fixture.componentInstance;
    // Setting the @Input() book value
    component.book = book;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  describe('onEdit', () => {

    it('should call updateBook service', () => {
      const expectedBook: Book = {
        id: 5,
        location: 'Dublin',
        time: '1553080742',
        author: 'Happy Manager',
        text: 'A modern PDF annotator that can accommodate all of the cooks in a very busy kitchen is what your employees really need.'
      };
      component.onEdit('Dublin', book, 'location');
      expect(mockBooksAPIService.updateBook).toHaveBeenCalledWith(expectedBook);
    });
  });
});
