import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-inline-edit',
  templateUrl: './inline-edit.component.html',
  styleUrls: ['./inline-edit.component.scss']
})
export class InlineEditComponent {
  @Input() data: string;
  @Output() edit: EventEmitter<string> = new EventEmitter<string>();
  editMode = false;
  constructor() { }

  onEdit() {
    this.edit.emit(this.data);
  }

}
