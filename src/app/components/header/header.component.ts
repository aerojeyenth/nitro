import { Component, ChangeDetectionStrategy } from '@angular/core';
import { Book } from 'src/app/services/books-api.service';

export type Key = 'author' | 'week' | 'location';

export interface GroupedBook {
  [key: string]: Book[];
}

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss'],
})
export class HeaderComponent {

  constructor() { }
}
