import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import _keyBy from 'lodash/keyBy';
import _values from 'lodash/values';
import { skip } from 'rxjs/operators';

export interface Book {
  id: number;
  location: string;
  time: any;
  author: string;
  text: string;
}

export interface BookEntity {
  [key: number]: Book;
}

const sampleData: Book[] = [
  {
    id: 1,
    location: 'San Francisco',
    time: '1552657573',
    author: 'Happy User',
    text: 'Proper PDF conversion ensures that every element of your document remains just as you left it.'
  },
  {
    id: 2,
    location: 'San Francisco',
    time: '1552571173',
    author: 'Happy User',
    text: 'The modern workplace is increasingly digital, and workflows are constantly evolving. '
  },
  {
    id: 3,
    location: 'San Francisco',
    time: '1552571174',
    author: 'Happy Developer',
    text: 'Digital transformation isn’t just a buzzword'
  },
  {
    id: 4,
    location: 'Sydney',
    time: '1552563973',
    author: 'Happy Developer',
    text: 'An expectation of digital efficiency has become the norm in our daily lives'
  },
  {
    id: 5,
    location: 'Dublin',
    time: '1553080742',
    author: 'Happy Manager',
    text: 'A modern PDF annotator that can accommodate all of the cooks in a very busy kitchen is what your employees really need.'
  },
  {
    id: 6,
    location: 'Dublin',
    time: '1553099742',
    author: 'Happy Manager',
    text: 'An integrated productivity solution breaks information through barriers and allows workers to collaborate in real time.'
  }
];


@Injectable({
  providedIn: 'root'
})
export class BooksAPIService {

  private booksStore$: BehaviorSubject<Book[]> = new BehaviorSubject(sampleData);
  private booksEntity$: BehaviorSubject<BookEntity> = new BehaviorSubject(null);

  constructor() {
    this.booksStore$.subscribe(books => this.booksEntity$.next( _keyBy(books, 'id')));
   }

  get books(): Observable<Book[]> {
    return this.booksStore$.asObservable();
  }

  public updateBook(value: Book) {
    const currentValue = this.booksEntity$.getValue();
    const update = {};
    update[value.id] = value;
    this.booksEntity$.next({...currentValue, ...update});
    this.booksStore$.next(_values(this.booksEntity$.getValue()));
  }
}
