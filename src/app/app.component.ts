import { Component, OnInit } from '@angular/core';
import { BooksAPIService, Book } from 'src/app/services/books-api.service';
import { combineLatest, BehaviorSubject, Observable } from 'rxjs';
import _groupby from 'lodash/groupby';
import getWeek from 'date-fns/getWeek';
import { map } from 'rxjs/operators';

export type Key = 'author' | 'week' | 'location';

export interface GroupedBook {
  [key: string]: Book[];
}

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {

  groupedData$: Observable<GroupedBook>;
  private groupByKey$: BehaviorSubject<Key> = new BehaviorSubject('week');
  constructor(private booksService: BooksAPIService) { }

  ngOnInit(): void {
    this.groupedData$ = combineLatest(this.booksService.books, this.groupByKey$).pipe(
      map(([booksData, key]) => {
        return _groupby(booksData, (n: Book) => key === 'week' ? `Week ${getWeek(+n.time * 1000)}` : n[key]);
      })
    );
  }

  get key(): Key {
    return this.groupByKey$.getValue();
  }

  groupby(key: Key): void {
    this.groupByKey$.next(key);
  }
}
