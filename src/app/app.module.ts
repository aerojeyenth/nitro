import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { HeaderComponent } from './components/header/header.component';
import { FilterComponent } from './components/filter/filter.component';
import { GroupBooksComponent } from './components/group-books/group-books.component';
import { BookCardComponent } from './components/book-card/book-card.component';
import { InlineEditComponent } from './components/inline-edit/inline-edit.component';
import { FormsModule } from '@angular/forms';

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    FilterComponent,
    GroupBooksComponent,
    BookCardComponent,
    InlineEditComponent
  ],
  imports: [
    BrowserModule,
    FormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
