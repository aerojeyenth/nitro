import { TestBed, async, ComponentFixture } from '@angular/core/testing';
import { AppComponent } from './app.component';
import { BooksAPIService, Book } from './services/books-api.service';
import { GroupedBook, HeaderComponent } from './components/header/header.component';
import { of, Observable } from 'rxjs';
import { DebugElement } from '@angular/core';
import { GroupBooksComponent } from './components/group-books/group-books.component';
import { FilterComponent } from './components/filter/filter.component';
import { BookCardComponent } from './components/book-card/book-card.component';
import { InlineEditComponent } from './components/inline-edit/inline-edit.component';

const sampleData: Book[] = [
  {
    id: 1,
    location: 'San Francisco',
    time: '1552657573',
    author: 'Happy User',
    text: 'Proper PDF conversion ensures that every element of your document remains just as you left it.'
  },
  {
    id: 2,
    location: 'San Francisco',
    time: '1552571173',
    author: 'Happy User',
    text: 'The modern workplace is increasingly digital, and workflows are constantly evolving. '
  },
  {
    id: 3,
    location: 'Dublin',
    time: '1552571174',
    author: 'Happy Developer',
    text: 'Digital transformation isn’t just a buzzword'
  }
];

const groupedByLocation: GroupedBook = {
  'San Francisco': [{
    id: 1,
    location: 'San Francisco',
    time: '1552657573',
    author: 'Happy User',
    text: 'Proper PDF conversion ensures that every element of your document remains just as you left it.'
  },
  {
    id: 2,
    location: 'San Francisco',
    time: '1552571173',
    author: 'Happy User',
    text: 'The modern workplace is increasingly digital, and workflows are constantly evolving. '
  }],
  Dublin: [{
    id: 3,
    location: 'Dublin',
    time: '1552571174',
    author: 'Happy Developer',
    text: 'Digital transformation isn’t just a buzzword'
  }]
};

describe('AppComponent', () => {
  let component: AppComponent;
  let fixture: ComponentFixture<AppComponent>;
  let booksAPIServiceServiceSpy: any;

  class MockBooksAPIService {
    get books() {
      return of(sampleData);
    }
    updateBook() {}
  }

  beforeEach(async(() => {
    // const spy = jasmine.createSpyObj('BooksAPIService', ['books']);

    TestBed.configureTestingModule({
      declarations: [
        AppComponent,
        HeaderComponent,
        GroupBooksComponent,
        FilterComponent,
        BookCardComponent,
        InlineEditComponent
      ],
      providers: [{provide: BooksAPIService, useClass: MockBooksAPIService}]
    }).compileComponents();

    booksAPIServiceServiceSpy = TestBed.inject(BooksAPIService);

  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AppComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create the app', () => {
    expect(component).toBeTruthy();
  });

  it('should group the books data by location', () => {
    component.groupby('location');
    component.groupedData$.subscribe((groupedBooks) => {
      expect(groupedBooks).toEqual(groupedByLocation);
    });
  });
});
